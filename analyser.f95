! (c) 2017, Konrad Jopek <kjopek@agh.edu.pl>
!
! This is Fortran 90/95 implementation of FLOPS estimator
! previously written in C++.

program flops_estimator
    use mesh_mod
    implicit none

    character(len=255) :: filename
    integer iargc

    if (iargc() /= 2) then
        write(*,*) "Usage flops_estimator -f <input tree>"
        call exit(1)
    end if

    call getarg(2, filename)
    call mesh_import(filename)
    call mesh_analyse_tree()
    call mesh_print_flops()
    call mesh_cleanup()
end program
