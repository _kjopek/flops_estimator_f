! Author: Konrad Jopek
! Mail: kjopek@agh.edu.pl, kjopek@gmail.com

! General comments on the style:
! * I generally use ii, jj, kk instead of i, j, k because it is much easier
!   to find two-letters iterators in the code using popular search.
! * I tend to separate logic fragments of the code using newlines. This may be
!   against typical convention in Fortran.
module mesh_mod
    use asserts

    implicit none

    public :: mesh_import, mesh_analyse_tree, mesh_cleanup, mesh_print_flops
    private :: MESH, ROOT_TREE_NODE, node_cleanup

    ! This structure represents mesh and contains global information
    ! about flops and supernodes:
    ! * nr_dofs - contains number of DOFs per mesh node.
    ! * nr_flops - contains number of flops per mesh node.
    ! * eliminate_at - id of tree_node where given supernode is eliminated.
    type mesh_node
        integer :: nr_flops = 0
        integer :: nr_dofs = 0
        integer :: eliminate_at = 0
    end type mesh_node

    ! This structure represents a node in partitioning tree.
    ! * nr_mesh_nodes_to_elim - represents number of supernodes that will be
    !                           eliminated on each node of the tree.
    !   Hence, supernodes_count - supernodes_to_elim = size of the Schur
    !                             complement.
    ! * parent - contains id of parent node.
    ! * children - contains ids of children nodes.
    ! * mesh_node_nr - is an array with supernodes present in given node.
    type tree_node
        integer                            :: parent = 0
        integer, allocatable, dimension(:) :: children
        integer, allocatable, dimension(:) :: mesh_node_nr
    end type tree_node

    ! Structures below are only for internal representation of tree in file.
    ! We do not use them during estimation of FLOPS.

    ! This structure is helper for reading elements id from file.
    ! * layer - is a adaptation level in case of FEM, for IGA should be 1.
    ! * element_id - is a unique element it within layer.
    type mesh_element_id
        integer :: layer = 0
        integer :: element_id = 0
    end type mesh_element_id

    ! This structure is used only to describe temporary structure called
    ! 'element' which is stored in file with tree. We will copy list of
    ! mesh_node_ids into tree_node%mesh_node_nr when we fully read input file.
    ! * id is fully described above.
    ! * nr_mesh_nodes - number of mesh nodes associated with element.
    ! * mesh_node_ids - list of mesh nodes associated with mesh_element.
    type mesh_element
        type(mesh_element_id)              :: id
        integer                            :: nr_mesh_nodes = 0
        integer, allocatable, dimension(:) :: mesh_node_ids
    end type mesh_element

    type(mesh_node), allocatable, dimension(:), target :: MESH
    type(tree_node), allocatable, dimension(:), target :: TREE_NODES
    integer :: ROOT_TREE_NODE

    contains

    subroutine mesh_import(filename)
    ! mesh_import imports mesh structure from file encoded in textual
    ! format.
        character(len=*), intent(in) :: filename
        integer :: mesh_node
        integer :: nr_mesh_nodes, nr_tree_nodes, tree_node_id
        integer :: id, fd, node_id, ii
        integer :: nr_elements, element, nr_elements_in_tree_node
        integer :: err_stat
        integer :: id_left_son, id_right_son
        type(mesh_element), allocatable, dimension(:) :: mesh_element_array
        type(mesh_element_id), allocatable, dimension(:) :: node_elements
        type(mesh_element), pointer :: node_element

        call assert(.not. allocated(MESH), "Mesh is already allocated.")

        fd = 40
        open(unit=fd, file=filename, action='read', form='formatted', &
            iostat=err_stat)
        call assert(err_stat == 0, "Could not open file")

        ! Read number of supernodes in mesh.
        read(fd, *) nr_mesh_nodes

        allocate(MESH(nr_mesh_nodes))

        ! Read number of DOFs per mesh node.
        do mesh_node = 1, nr_mesh_nodes
            read(fd, *) id, MESH(id)%nr_dofs
        end do

        ! Read number of elements included in file.
        read(fd, *) nr_elements
        allocate(mesh_element_array(nr_elements))

        ! Iterate over elements, and fill up mesh_element_array.
        do element = 1, nr_elements
            allocate(mesh_element_array(element)%mesh_node_ids(nr_mesh_nodes))
            read(fd, *) mesh_element_array(element)%id%layer, &
                mesh_element_array(element)%id%element_id, &
                mesh_element_array(element)%nr_mesh_nodes, &
                (mesh_element_array(element)%mesh_node_ids(node_id), &
                    node_id = 1, mesh_element_array(element)%nr_mesh_nodes)
        end do

        read(fd, *) nr_tree_nodes
        ! Prealocate structures. node_elements is temporary structure only for
        ! storing elements associated with given node. It is not interesting for
        ! us, as we are only interesting in leaves of the tree. Information of
        ! degrees of freedom will be fulfilled by analysis step.
        allocate(TREE_NODES(nr_tree_nodes))
        allocate(node_elements(nr_elements))

        ! Read the tree. Tree in file is composed from elements, so we need to
        ! translate elements into mesh nodes lists. Here, we convert only leafs
        ! as the rest will be done in analysis stage.
        do tree_node_id = 1, nr_tree_nodes
            ! We cannot advance here as we do not know the length of the input
            ! vector.
            read(fd, advance='NO', fmt='(2I12)') id, nr_elements_in_tree_node

            ! We can easily notice if tree file is corrupted.
            call assert(nr_elements_in_tree_node <= nr_elements, &
                "Number of elements in tree node is larger than number of &
                &elements in mesh")

            ! Root node should be stored first.
            if (ROOT_TREE_NODE == 0) then
                ROOT_TREE_NODE = id
            end if

            if (nr_elements_in_tree_node == 1) then
                read(fd, *) (node_elements(node_id), node_id = 1, &
                    nr_elements_in_tree_node)

                nullify(node_element)
                node_element => find_mesh_element(mesh_element_array, &
                    node_elements(1))

                ! Another check - node may contain only elements that were
                ! defined earlier.
                call assert(associated(node_element), &
                    "Invalid tree. Node contains unknown element.")

                ! Allocate mesh node list for leafs only right now. Analysis
                ! stage will allocate lists for other tree nodes.
                allocate(TREE_NODES(id)%mesh_node_nr(node_element% &
                        nr_mesh_nodes))
                do ii = lbound(TREE_NODES(id)%mesh_node_nr, 1), &
                    ubound(TREE_NODES(id)%mesh_node_nr, 1)
                    TREE_NODES(id)%mesh_node_nr(ii) = &
                        node_element%mesh_node_ids(ii)
                end do
            else
                read(fd, *) (node_elements(node_id), node_id = 1, &
                    nr_elements_in_tree_node), id_left_son, id_right_son
                ! Make sure that tree is correct. Both sons IDs must be within
                ! borders of TREE_NODES array.
                call assert(id_left_son >= lbound(TREE_NODES, 1), &
                    "Invalid tree. Left son is out of bounds.")
                call assert(id_left_son <= ubound(TREE_NODES, 1), &
                    "Invalid tree. Left son is out of bounds.")
                call assert(id_right_son >= lbound(TREE_NODES, 1), &
                    "Invalid tree. Right son is out of bounds.")
                call assert(id_right_son <= ubound(TREE_NODES, 1), &
                    "Invalid tree. Right son is out of bounds.")
                allocate(TREE_NODES(id)%children(2))
                TREE_NODES(id)%children(1) = id_left_son
                TREE_NODES(id)%children(2) = id_right_son
                TREE_NODES(id_left_son)%parent = id
                TREE_NODES(id_right_son)%parent = id
            end if
        end do

        deallocate(node_elements)
        deallocate(mesh_element_array)
        close(fd)
    end subroutine

    function find_mesh_element(mesh_element_array, id)
        type(mesh_element), dimension(:), target :: mesh_element_array
        type(mesh_element), pointer :: find_mesh_element
        type(mesh_element_id) :: id
        integer :: ii

        ! Shortcut - if elements are sorted we can theoretically find element
        ! in array in O(1) time.

        find_mesh_element => mesh_element_array(id%element_id)
        if (find_mesh_element%id%layer == id%layer .and. &
            find_mesh_element%id%element_id == id%element_id) then
           ! Exit function as we found corresponding element
           return
        end if

        ! Iterate over array, find matching element
        do ii = lbound(mesh_element_array, 1), ubound(mesh_element_array, 1)
            find_mesh_element => mesh_element_array(ii)
            if (find_mesh_element%id%layer == id%layer .and. &
                find_mesh_element%id%element_id == id%element_id) then
                ! Exit function as we found corresponding element
                return
            end if
        end do
        ! Return NULL if element does not exist.
        nullify(find_mesh_element)
    end function

    function lowest_common_ancestor(node1, node2, visited_nodes, stack)
    ! lowest_common_ancestor returns LCA of two nodes: node1 and node2
    ! using visited_nodes and stack arrays as a workspace.
    ! Using this method we can quickly O(log(number of tree_nodes)) get common
    ! ancestor.
        integer :: lowest_common_ancestor, node1, node2, node_iterator
        logical, dimension(:), pointer :: visited_nodes
        integer, dimension(:), pointer :: stack
        integer :: stack_pointer

        stack_pointer = 1
        stack(stack_pointer) = ROOT_TREE_NODE
        lowest_common_ancestor = ROOT_TREE_NODE
        visited_nodes(ROOT_TREE_NODE) = .true.

        ! Generate path to root from node1.
        node_iterator = node1
        do while (node_iterator /= 0)
            visited_nodes(node_iterator) = .true.
            stack_pointer = stack_pointer + 1
            stack(stack_pointer) = node_iterator
            node_iterator = TREE_NODES(node_iterator)%parent
        end do

        ! Scan path to root from node2, if there is any common point, save it
        ! and stop iteration.
        node_iterator = node2
        do while (node_iterator /= 0)
            if (visited_nodes(node_iterator)) then
                lowest_common_ancestor = node_iterator
                goto 10
                exit
            end if
            stack_pointer = stack_pointer + 1
            stack(stack_pointer) = node_iterator
            node_iterator = TREE_NODES(node_iterator)%parent
        end do

        ! Ok, we need to clean visited_nodes and stack after computations.
10      do while (stack_pointer > 0)
            visited_nodes(stack(stack_pointer)) = .false.
            stack_pointer = stack_pointer - 1
        end do
    end function

    recursive subroutine associate_dofs_with_nodes(node_id, visited_nodes, stack)
    ! Previous method of mapping dofs to tree nodes was too expensive. This is
    ! reimplementation that should be at most #dofs x log(tree size).
        integer, intent(in) :: node_id
        integer :: ii, child_id
        type(tree_node), pointer :: node
        type(mesh_node), pointer :: supernode => null()
        logical, dimension(:), pointer :: visited_nodes
        integer, dimension(:), pointer :: stack

        ! Go to tree leafs and collect information to which tree nodes
        ! given dofs belong to.
        node => TREE_NODES(node_id)
        if (.not. allocated(node%children)) then
            do ii = lbound(node%mesh_node_nr, 1), ubound(node%mesh_node_nr, 1)
                ! In leafs, we have a list of supernodes, so we need to find
                ! LCA for all of them. This information is enough to estimate
                ! FLOPS as we will push all unknowns toward root of the tree.

                supernode => MESH(node%mesh_node_nr(ii))
                if (supernode%eliminate_at == 0) then
                    supernode%eliminate_at = node_id
                else
                    supernode%eliminate_at = lowest_common_ancestor(node_id, &
                        supernode%eliminate_at, visited_nodes, stack)
                end if
            end do
        else
            do ii = lbound(node%children, 1), ubound(node%children, 1)
                child_id = node%children(ii)
                call associate_dofs_with_nodes(child_id, visited_nodes, stack)
            end do
        end if
    endsubroutine

    function nr_supernodes_to_copy(node_id, supernodes_seen)
    ! nr_supernodes_to_copy estimates number of supernodes to be copied from
    ! all children of tree node with id = node_id.
    ! After this function you have to call clear_supernodes_seen!
        integer, intent(in) :: node_id
        logical, dimension(:), pointer :: supernodes_seen
        integer :: nr_supernodes_to_copy
        integer :: ii, supernode_id
        type(tree_node), pointer :: node

        node => TREE_NODES(node_id)
        nr_supernodes_to_copy = 0
        do ii = lbound(node%mesh_node_nr, 1), ubound(node%mesh_node_nr, 1)
            supernode_id = node%mesh_node_nr(ii)

            if (MESH(supernode_id)%eliminate_at /= node_id .and. &
                .not. supernodes_seen(supernode_id)) then
                nr_supernodes_to_copy = nr_supernodes_to_copy + 1
                supernodes_seen(supernode_id) = .true.
            end if
        end do
    end function

    subroutine copy_supernodes(node_id, supernodes_seen)
    ! Copy supernodes from children to array mesh_node_nr in given node.
    ! Please notice, that only needed supernodes will be copied. This function
    ! must ignore supernodes that are eliminated in any of the children.
        integer, intent(in) :: node_id
        logical, dimension(:), pointer :: supernodes_seen
        integer :: array_idx, child_id, supernode_id
        type(tree_node), pointer :: node
        type(tree_node), pointer :: child
        integer :: ii, jj

        node => TREE_NODES(node_id)
        array_idx = 1

        ! Iterate over all of the children
        do ii = lbound(node%children, 1), ubound(node%children, 1)
            child_id = node%children(ii)
            child => TREE_NODES(child_id)

            do jj = lbound(child%mesh_node_nr, 1), ubound(child%mesh_node_nr, 1)
                supernode_id = child%mesh_node_nr(jj)
                ! Add only those supernodes that are not eliminated in any of
                ! the children and were not already added.
                if (MESH(supernode_id)%eliminate_at /= child_id .and. &
                    .not. supernodes_seen(supernode_id)) then
                    node%mesh_node_nr(array_idx) = supernode_id
                    array_idx = array_idx + 1
                    supernodes_seen(supernode_id) = .true.
                end if
            end do
        end do

        ! We have to call clear_supernodes_seen.
        do ii = lbound(node%children, 1), ubound(node%children, 1)
            child_id = node%children(ii)
            call clear_supernodes_seen(child_id, supernodes_seen)
        end do
    end subroutine

    subroutine clear_supernodes_seen(node_id, supernodes_seen)
    ! This is very important function that clears supernodes_seen array.
    ! supernodes_seen array is helper data structure to speed up analysis stage.
    ! Without proper clearing it directly leads to serious and hard to find
    ! bugs.
        integer, intent(in) :: node_id
        integer :: ii, supernode_id
        logical, dimension(:), pointer :: supernodes_seen
        type(tree_node), pointer :: node

        node => TREE_NODES(node_id)

        do ii = lbound(node%mesh_node_nr, 1), ubound(node%mesh_node_nr, 1)
            supernode_id = node%mesh_node_nr(ii)
            supernodes_seen(supernode_id) = .false.
        end do
    end subroutine

    function needed_array_size(node_id, supernodes_seen)
    ! This function estimates size of array for storing supernodes at the level
    ! of single tree node.
        integer, intent(in) :: node_id
        logical, dimension(:), pointer :: supernodes_seen
        integer :: needed_array_size
        integer :: ii
        type(tree_node), pointer :: node

        node => TREE_NODES(node_id)

        needed_array_size = 0
        do ii = lbound(node%children, 1), ubound(node%children, 1)
            needed_array_size = needed_array_size + &
                nr_supernodes_to_copy(node%children(ii), supernodes_seen)
        end do

        do ii = lbound(node%children, 1), ubound(node%children, 1)
            call clear_supernodes_seen(node%children(ii), supernodes_seen)
        end do
    end function

    recursive subroutine node_perform_dofs_analysis(node_id, supernodes_seen)
        integer, intent(in) :: node_id
        logical, dimension(:), pointer :: supernodes_seen
        integer :: child_node_id, ii
        type(tree_node), pointer :: node

        node => TREE_NODES(node_id)
        if (allocated(node%children)) then
            do ii = lbound(node%children, 1), ubound(node%children, 1)
                child_node_id = node%children(ii)
                call node_perform_dofs_analysis(child_node_id, supernodes_seen)
            end do

            allocate(node%mesh_node_nr(needed_array_size(node_id, &
                supernodes_seen)))
            call copy_supernodes(node_id, supernodes_seen)
        end if

        call node_perform_flops_analysis(node_id)
    end subroutine

    subroutine node_perform_flops_analysis(node_id)
    ! This subroutine compute number of flops for every supernode in given tree
    ! node identified by node_id.
        integer, intent(in) :: node_id
        integer :: schur_size, factorization_size
        integer :: ii, mesh_node_id, local_flops, nr_dofs
        type(tree_node), pointer :: node

        node => TREE_NODES(node_id)
        schur_size = 0
        factorization_size = 0
        ! It is important here to notice, that supernodes are basically
        ! "set of dofs". We need to take it into consideration while computing
        ! number of flops. This comment does not apply to IGA, where typically
        ! supernode contains only single DOF. However, for FEM analysis that
        ! change is crucial.

        ! First pass - compute size of schur complement and size of matrix
        ! to factorize.
        call assert(allocated(node%mesh_node_nr), &
            "mesh_node_nr field is not allocated.")
        do ii = lbound(node%mesh_node_nr, 1), ubound(node%mesh_node_nr, 1)
            mesh_node_id = node%mesh_node_nr(ii)
            nr_dofs = MESH(mesh_node_id)%nr_dofs

            if (MESH(mesh_node_id)%eliminate_at == node_id) then
                factorization_size = factorization_size + nr_dofs
            else
                schur_size = schur_size + nr_dofs
            end if
        end do

        ! Update number of flops per supernode.
        do ii = lbound(node%mesh_node_nr, 1), ubound(node%mesh_node_nr, 1)
            mesh_node_id = node%mesh_node_nr(ii)
            nr_dofs = MESH(mesh_node_id)%nr_dofs

            ! Compute number of flops. We multiply estimated number of flops by
            ! number of DOFs associated with given supernode.
            local_flops = nr_dofs * mesh_get_flops(factorization_size, &
                schur_size, MESH(mesh_node_id)%eliminate_at == node_id)
            MESH(mesh_node_id)%nr_flops = MESH(mesh_node_id)%nr_flops + &
                local_flops
        end do
    end subroutine

    subroutine mesh_analyse_tree()
    ! This function takes the root of the tree, walking down to leafs and start
    ! analysis from leafs. It has two stages. Firstly, it finds out at which
    ! node of the tree the given supernode is eliminated. Then, using that
    ! knowledge, it estimates number of flops required to factorize matrix.
        ! Many of the variables below are only for optimization. visited_nodes,
        ! supernodes_seen and stack allowed me to use perform constant-time
        ! operations instead of O(log N) or O(N).
        logical, dimension(ubound(TREE_NODES, 1)), target :: visited_nodes
        logical, dimension(:), pointer :: visited_nodes_ptr
        logical, dimension(ubound(MESH, 1)), target :: supernodes_seen
        logical, dimension(:), pointer :: supernodes_seen_ptr
        integer, dimension(ubound(TREE_NODES, 1)), target :: stack
        integer, dimension(:), pointer :: stack_ptr

        ! Create empty set and pass it to the analysis function for root node.
        stack = 0
        visited_nodes = .false.
        ! Strange hack to convert array to pointer.
        visited_nodes_ptr => visited_nodes
        stack_ptr => stack
        call associate_dofs_with_nodes(ROOT_TREE_NODE, visited_nodes_ptr, &
            stack_ptr)
        ! Second stage, actual flops analysis.
        supernodes_seen = .false.
        supernodes_seen_ptr => supernodes_seen
        call node_perform_dofs_analysis(ROOT_TREE_NODE, supernodes_seen_ptr)
    end subroutine

    function mesh_get_flops(a, b, is_eliminated)
    ! a and b are the number of eliminated supernodes (group of DOFs)
    ! and schur complement size respectively.
    ! is_eliminated informs if given supernode belongs to schur complement
    ! or is eliminated.

        integer :: a, b, mesh_get_flops
        logical :: is_eliminated
        ! The number of flops depends on where are we in the matrix. For
        ! supernodes that are eliminated we need to add flops from LU
        ! factorization and computing A^(-1)B. While for supernodes
        ! from Schur complement we need to add flops from computing:
        ! A^(-1)B, C x (A^(-1)B) and D -= C x (A^(-1)B)
        ! The equations below can be easily obtained from my document about
        ! complexity of LU factorization.
        call assert(a >= 0, "a is negative")
        call assert(b >= 0, "b is negative")
        if (is_eliminated) then
            mesh_get_flops = ceiling((4 * a ** 2 - 3 * a + 5) / 6.0) + &
                b * (2 * a - 1)
        else
            mesh_get_flops = (2 * a + 2) * b
        end if
    end function

    subroutine mesh_print_flops()
    ! Print computed flops.
        integer :: dof
        ! Iterate over mesh and print number of flops for every dof/supernode.
        ! In case of IGA we should have always 1 dof per 1 supernode.
        ! Supernode means part of the mesh element like face, vertex, edge, etc.
        do dof = lbound(MESH, 1), ubound(MESH, 1)
            write(*,*) dof - 1, ' = ', MESH(dof)%nr_flops
        end do
    end subroutine

    recursive subroutine node_cleanup(node)
    ! Cleanup the tree.
        type(tree_node), pointer :: node
        type(tree_node), pointer :: son_left, son_right
        ! Avoid dereference null pointer.
        if (.not. associated(node)) then
            return
        end if

        ! Recursively free left and right son.
        if (allocated(node%children)) then
            son_left => TREE_NODES(node%children(1))
            son_right => TREE_NODES(node%children(2))
            call node_cleanup(son_left)
            call node_cleanup(son_right)
        end if

        ! Deallocate tables related to node_ and its fields.
        if (allocated(node%mesh_node_nr)) then
            deallocate(node%mesh_node_nr)
        end if

        if (allocated(node%children)) then
            deallocate(node%children)
        end if
    end subroutine

    subroutine mesh_cleanup()
    ! Cleanup mesh, free all resources.
        type(tree_node), pointer :: root
        ! Free resource.
        ! Free partitioning tree.
        root => TREE_NODES(ROOT_TREE_NODE)
        call assert(allocated(MESH), "Mesh is not allocated")
        call node_cleanup(root)
        deallocate(TREE_NODES)
        ! Free previously allocated array within mesh structure.
        deallocate(MESH)
    end subroutine
end module
