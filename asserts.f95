! Author: Konrad Jopek
! Mail: kjopek@gmail.com, kjopek@agh.edu.pl
module asserts
    implicit none
    contains
    subroutine assert(condition, message)
    ! Typical assert function that is well known from C programmes
    ! Terminate program if condition is not met and dump coredump for later
    ! analysis.
        logical :: condition
        character(len=*) :: message
        if (.not. condition) then
            write(0, *) message
            call abort
        end if
    end subroutine assert
end module asserts
