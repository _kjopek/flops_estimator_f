PROG= analyser
SRCS= asserts.f95 mesh.f95 analyser.f95

FORT?= gfortran6
FFLAGS= -g -O2 -Wall -Wextra -Warray-temporaries -Wconversion \
		-fbacktrace -ffree-line-length-0
# for Intel Compiler comment above line and uncomment the following line:
# FORT= ifort

$(PROG): $(SRCS)
	$(FORT) $(FFLAGS) $(SRCS) -o $(PROG)

clean:
	rm -f $(PROG) *.o *.mod
